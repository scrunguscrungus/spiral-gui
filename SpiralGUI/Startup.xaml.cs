﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Net;
using Microsoft.Win32;
using System.Diagnostics;

namespace SpiralGUI
{
	public class StepChangedEventArgs : EventArgs
	{
		public StartupStep newStep { get; set;  }
	}
	public class ProgressUpdateEventArgs : EventArgs
	{
		public int NewProgress { get; set; }
	}
	public abstract class StartupStep
	{
		abstract public string Name { get; }
		virtual public List<StartupStep> Substeps { get; }

		public StartupStep CurrentStep;


		public event EventHandler<StepChangedEventArgs> StepChanged;
		public event EventHandler<ProgressUpdateEventArgs> ProgressUpdated;

		protected virtual void OnStepChanged(StepChangedEventArgs args)
		{
			StepChanged?.Invoke(this, args);
		}
		protected virtual void OnProgressUpdate(ProgressUpdateEventArgs args)
		{
			ProgressUpdated?.Invoke(this, args);
		}

		

		public virtual async Task PerformStep()
		{
			await DoSubSteps();
		}
		async Task DoSubSteps()
		{
			StepChangedEventArgs args = new StepChangedEventArgs();
			if ( Substeps != null )
			{
				foreach (StartupStep step in Substeps)
				{
					step.StepChanged += StepChanged;
					step.ProgressUpdated += ProgressUpdated;
					if (step.ShouldPerformAutomatically())
					{
						args.newStep = step;
						OnStepChanged(args);
						await step.PerformStep();
					}
				}
			}
		}

		virtual public bool ShouldPerformAutomatically()
		{
			return true;
		}
	}

	//Main startup, encompasses all other steps
	public class MainStartupStep : StartupStep
	{
		public override string Name => "Starting up...";

		public override List<StartupStep> Substeps
		{
			get
			{
				return new List<StartupStep>()
				{
					new CheckDependenciesStep(),
					new DoneStep()
				};
			}
		}
	}

	public class DoneStep : StartupStep
	{
		public override string Name => "Done!";
	}

	//Dependency checks
	public class CheckDependenciesStep : StartupStep
	{
		public override string Name => "Checking Dependencies...";

		public override List<StartupStep> Substeps
		{
			get
			{
				return new List<StartupStep>()
				{
					new CheckSpiralDependencyStep(),
					new CheckJavaDependencyStep()
				};
			}
		}
	}

	public class CheckSpiralDependencyStep : StartupStep
	{
		public override string Name => "Checking for SPIRAL...";
		public override List<StartupStep> Substeps => new List<StartupStep>();


		public override async Task PerformStep()
		{
			await base.PerformStep();

			if (!File.Exists("./spiral.jar"))
			{
				DownloadFileStep dfs = new DownloadFileStep(@"https://jenkins.abimon.org/job/Spiral-Console/lastSuccessfulBuild/artifact/console/build/libs/spiral-console-shadow.jar", "./spiral.jar");
				dfs.StepChanged += StepChangeDL;
				dfs.ProgressUpdated += ProgressChangeDL;
				await dfs.PerformStep();
			}
		}

		public void StepChangeDL(object s, StepChangedEventArgs a)
		{
			OnStepChanged(a);
		}
		public void ProgressChangeDL(object s, ProgressUpdateEventArgs a)
		{
			OnProgressUpdate(a);
		}
	}

	public class CheckJavaDependencyStep : StartupStep
	{
		public override string Name => "Checking for Java...";
		
		public override List<StartupStep> Substeps => new List<StartupStep>();

		public override async Task PerformStep()
		{
			await base.PerformStep();

			string javaPath = Utils.GetJavaInstallationPath();

			if (true)
			{
				string downloadPath = String.Empty;
				if ( Environment.Is64BitOperatingSystem )
				{
					downloadPath = @"https://download.oracle.com/otn/java/jdk/8u221-b11/230deb18db3e4014bb8e3e8324f81b43/jre-8u221-windows-x64.exe";
				}
				else
				{
					downloadPath = @"https://download.oracle.com/otn/java/jdk/8u221-b11/230deb18db3e4014bb8e3e8324f81b43/jre-8u221-windows-i586.exe";
				}
				string filename = Path.GetFileName(downloadPath);

				DownloadFileStep dfs = new DownloadFileStep(downloadPath, $"./{filename}");
				dfs.StepChanged += StepChangeDL;
				dfs.ProgressUpdated += ProgressChangeDL;
				await dfs.PerformStep();

				StepChangedEventArgs args = new StepChangedEventArgs();
				args.newStep = this;
				OnStepChanged(args);

				bool bJavaInstalled = false;
				while ( !bJavaInstalled )
				{
					Process jInstall = Process.Start(filename);
					jInstall.WaitForExit();

					if ( Utils.GetJavaInstallationPath() != null )
					{
						bJavaInstalled = true;
					}
					else
					{
						MessageBoxResult reply = MessageBox.Show("The Java installation appears to have failed. Retry?", "Installation Failed", MessageBoxButton.YesNo, MessageBoxImage.Error, MessageBoxResult.Yes);
						if ( reply == MessageBoxResult.No )
						{
							Environment.Exit(-1);
						}
					}
				}
			}
		}

		public void StepChangeDL(object s, StepChangedEventArgs a)
		{
			OnStepChanged(a);
		}
		public void ProgressChangeDL(object s, ProgressUpdateEventArgs a)
		{
			OnProgressUpdate(a);
		}
	}

	public class DownloadFileStep : StartupStep
	{
		public string FileURL;
		public string FileTargetLocation;
		public bool bFinished;
		public override string Name { get { return $"Downloading {Path.GetFileName(FileURL)}"; } }

		public DownloadFileStep(string url, string target)
		{
			FileURL = url;
			FileTargetLocation = target;
		}

		public override async Task PerformStep()
		{
			await base.PerformStep();
			StepChangedEventArgs args = new StepChangedEventArgs();
			args.newStep = this;
			OnStepChanged(args);

			WebClient wc = new WebClient();
			wc.DownloadProgressChanged += UpdateProgress;
			wc.DownloadFileCompleted += DownloadComplete;
			wc.DownloadFileAsync(new Uri(FileURL), FileTargetLocation);

			while ( !bFinished )
			{
				await Task.Delay(1);
			}
		}

		public void DownloadComplete(object s, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			bFinished = true;
		}

		public void UpdateProgress(object sender, DownloadProgressChangedEventArgs e)
		{
			ProgressUpdateEventArgs args = new ProgressUpdateEventArgs();
			args.NewProgress = e.ProgressPercentage;
			OnProgressUpdate(args);
		}

		public override bool ShouldPerformAutomatically()
		{
			return false;	
		}
	}

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class StartupWindow : Window
	{
		public StartupWindow()
		{
			InitializeComponent();
			Startup();
		}

		private async void Startup()
		{
			MainStartupStep mainStartup = new MainStartupStep();
			lblStatus.Content = mainStartup.Name;
			mainStartup.StepChanged += MainStartup_StepChanged;
			mainStartup.ProgressUpdated += MainStartup_ProgressUpdated;
			await mainStartup.PerformStep();
		}

		private void MainStartup_ProgressUpdated(object sender, ProgressUpdateEventArgs e)
		{
			prgCurrentStep.Value = e.NewProgress;
		}

		private void MainStartup_StepChanged(object sender, StepChangedEventArgs e)
		{
			lblStatus.Content = e.newStep.Name;
		}
	}
}
